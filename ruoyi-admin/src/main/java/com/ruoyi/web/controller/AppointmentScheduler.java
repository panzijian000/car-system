package com.ruoyi.web.controller;

import com.ruoyi.system.domain.Emnu.AppointmentStatus;
import com.ruoyi.system.domain.pojo.DriverAppointment;
import com.ruoyi.system.mapper.DriverAppointmentMapper;
import com.ruoyi.system.mapper.SysDictDataMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.io.UnsupportedEncodingException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;

@Component
public class AppointmentScheduler {

    private static final Logger logger = Logger.getLogger(AppointmentScheduler.class.getName());

    private final DriverAppointmentMapper driverAppointmentMapper;

    @Autowired
    private SysDictDataMapper sysDictDataMapper;

    @Autowired
    private WxSendMessage wxSendMessage; // 使用了字段注入

    // 通过构造器注入Mapper
    public AppointmentScheduler(DriverAppointmentMapper driverAppointmentMapper) {
        this.driverAppointmentMapper = driverAppointmentMapper;
    }

    @PostConstruct
    private void init() {
        // 初始化代码，如果有的话
    }

    @Scheduled(cron = "0/10 * * * * ?") // 每10秒执行一次
    public void cancelUnpaidOrders() {
        logger.info("执行预约超时定时任务");
        List<DriverAppointment> driverAppointmentList = fetchDriverAppointments();
        if (CollectionUtils.isEmpty(driverAppointmentList)) {
            logger.fine("没有可处理的预约");
            return;
        }

        List<DriverAppointment> expiredOrders = filterExpiredOrders(driverAppointmentList);
        if (!expiredOrders.isEmpty()) {
            notifyExpiredOrders(expiredOrders);
            updateAppointmentStatus(expiredOrders);
        }
    }

    private List<DriverAppointment> fetchDriverAppointments() {
        try {
            return driverAppointmentMapper.DriverList();
        } catch (Exception e) {
            logger.severe("无法获取预约列表: " + e.getMessage());
            return Collections.emptyList(); // 返回空列表而不是null
        }
    }

    private void notifyExpiredOrders(List<DriverAppointment> expiredOrders) {
        List<Integer> expiredOrderIds = mapToOrderIds(expiredOrders);
        expiredOrderIds.forEach(this::sendExpireNotification);
    }

    private void sendExpireNotification(Integer orderId) {
        try {
            driverAppointmentMapper.updateStatusById(orderId,AppointmentStatus.EXPIRED.getCode());
            wxSendMessage.sendMsg(orderId);
        } catch (UnsupportedEncodingException e) {
            logger.severe("发送消息失败: Order ID " + orderId + " - " + e.getMessage());
        }
    }

    private void updateAppointmentStatus(List<DriverAppointment> expiredOrders) {
        try {
            List<Integer> expiredOrderIds = mapToOrderIds(expiredOrders);
            driverAppointmentMapper.batchUpdateStatus(expiredOrderIds, AppointmentStatus.EXPIRED.getCode());
            logger.info("成功更新超时预约状态");
        } catch (Exception e) {
            logger.severe("批量更新状态失败: " + e.getMessage());
        }
    }

    private List<Integer> mapToOrderIds(List<DriverAppointment> appointments) {
        return appointments.stream().map(DriverAppointment::getId).collect(Collectors.toList());
    }

    private List<DriverAppointment> filterExpiredOrders(List<DriverAppointment> driverAppointmentList) {
        return driverAppointmentList.stream()
                .filter(order -> order.getStatus() == AppointmentStatus.ADMITTED.getCode())
                .filter(this::isOrderExpired)
                .collect(Collectors.toList());
    }

    private boolean isOrderExpired(DriverAppointment order) {
        LocalDateTime orderDateTime = convertToDate(order.getContactTime());
        return orderDateTime.plusMinutes(sysDictDataMapper.selectOverTimeByDictCode()).isBefore(LocalDateTime.now());
    }

    private LocalDateTime convertToDate(Date date) {
        return date.toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDateTime();
    }
}