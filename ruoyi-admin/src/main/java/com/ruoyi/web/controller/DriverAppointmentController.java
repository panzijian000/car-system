package com.ruoyi.web.controller;


import com.ruoyi.common.annotation.Anonymous;
import com.ruoyi.common.core.domain.entity.SysDept;
import com.ruoyi.common.core.domain.entity.SysDictData;
import com.ruoyi.system.domain.Emnu.AppointmentStatus;
import com.ruoyi.system.domain.pojo.DriverAppointment;
import com.ruoyi.system.domain.pojo.EventInfo;
import com.ruoyi.system.domain.pojo.orderLine;
import com.ruoyi.system.mapper.DriverAppointmentMapper;
import com.ruoyi.system.mapper.SysDeptMapper;
import com.ruoyi.system.mapper.SysDictDataMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.core.domain.AjaxResult;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;


/**
 * 司机进厂登录
 *
 * @author ruoyi
 */
@RestController
@Anonymous
public class DriverAppointmentController
{



    @Autowired
    DriverAppointmentMapper driverAppointmentMapper;

    @Autowired
    SysDeptMapper sysDeptMapper;
    @Autowired
    SysDictDataMapper sysDictDataMapper;
    /**
     * 获取进厂的事由
     */
    @Anonymous
    @GetMapping("/selectReasonList")
    public List<SysDictData> selectReasonList(){
        return  sysDictDataMapper.selectReasonListByType("appo_matter");
    }

    /**
     * 获取所有污染等级
     */
    @Anonymous
    @GetMapping("/emiStandardtList")
    public List<SysDictData> selectEmiStandardtList()
    {
        return    sysDictDataMapper.selectDictDataByType("appo_emi_standard");
    }
    /**
     * 查询所有部门，名称和ID
     */
    @Anonymous
    @GetMapping("/sysDeptList")
    public List<SysDept> selectDeptListNameAndId()
    {
        return    sysDeptMapper.selectDeptListNameAndId();
    }

    /**
     * 根据id更新信息
     */
    @PutMapping("/updateDriverById")
    public int  updateDriverById(@RequestBody EventInfo eventInfo)
    {
//        eventInfo.setDept_id(sysDeptMapper.selectDeptIdByName(eventInfo.getProductionLine()));
            return     driverAppointmentMapper.updateById(eventInfo);
    }

    /**
     * 根据id查询预约记录
     */
    @GetMapping("/selectDriverById")
    public DriverAppointment  selectDriverById(@RequestParam("id") int id)
    {
        return  driverAppointmentMapper.selectDriverById(id);
    }


    /**
     * 根据id修改状态
     */
    @GetMapping("/updateStatusById")
    public int updateStatusById(@RequestParam("id") int id,@RequestParam("status") int status)
    {
        return  driverAppointmentMapper.updateStatusById(id, status)    ;
    }

    /**
     * 根据openid判断是否有已预约的记录
     */
    @GetMapping("/ifcommit")
    public int  ifcommit(@RequestParam("openid")String openid)
    {
        List<DriverAppointment> driverAppointmentList = driverAppointmentMapper.DriverListByOpenId(openid);

        boolean hasStatusZero = driverAppointmentList.stream()
                .anyMatch(order -> order.getStatus() ==AppointmentStatus.SCHEDULED.getCode());

        if (hasStatusZero) {
            // 集合中至少有一个元素的状态为1
            return 1;
        } else {
            // 集合中没有元素的状态为0
            return 0;
        }
    }

    /**
     * 司机列表
     */
    @GetMapping("/getDriverList")
    public AjaxResult getDriverList()
    {
      return  AjaxResult.success(driverAppointmentMapper.DriverList());
    }

    /**
     * 司机列表根据openid查询
     * 包含排名信息
     */
    @GetMapping("/getDriverListByOpenid")
    public AjaxResult getDriverListByOpenid(@RequestParam("openid") String openid) {
        System.out.println("查询用的openid是=" + openid);
        List<DriverAppointment> driverAppointmentList = driverAppointmentMapper.DriverListByOpenId(openid);
        List<orderLine> orderLineTotals = driverAppointmentMapper.totalCountByCondition();

        for (DriverAppointment driverAppointment : driverAppointmentList) {
            if (driverAppointment.getStatus() == 3 || driverAppointment.getStatus() == 0) {
                // 假设orderLineTotals中已经包含了基于targetLine和reason的总数
                for (orderLine total : orderLineTotals) {
                    if (total.getTargetLine().equals(driverAppointment.getTargetLine()) &&
                            total.getReason().equals(driverAppointment.getReason())) {
                        driverAppointment.setOrderLineTotal(total.getOrderLineTotal());
                        break; // 找到匹配项后退出循环
                    }
                }
                // 假设countByCondtion是根据createdAt获取orderLine
                orderLine orderLine = driverAppointmentMapper.countByCondtion(driverAppointment.getCreatedAt(),driverAppointment.getTargetLine(),driverAppointment.getReason());
                if (orderLine != null) {
                    driverAppointment.setOrderLine(orderLine.getOrderLine());
                }
            }
        }

        return AjaxResult.success(driverAppointmentList);
    }

    /**
     * 提交司机表单
     */

    @PutMapping("/sumbitDriver")
    public AjaxResult sumbitDriver(@RequestBody EventInfo eventInfo){
        // 获取当前时间
        LocalDateTime now = LocalDateTime.now();
        // 将LocalDateTime转换为Timestamp
        Timestamp currentTimestamp = Timestamp.valueOf(now);
        DriverAppointment driverAppointment = new DriverAppointment();
        driverAppointment.setCreatedAt(currentTimestamp);
        driverAppointment.setOpenid(eventInfo.getOpenid());
        driverAppointment.setName(eventInfo.getName());
        driverAppointment.setCompany(eventInfo.getCompany());
        driverAppointment.setContactPhone(eventInfo.getPhone());
        driverAppointment.setContainerNum(eventInfo.getContainerNum());
        driverAppointment.setSealNum(eventInfo.getSealNum());
        driverAppointment.setTargetLine(eventInfo.getProductionLine());
        driverAppointment.setReason(eventInfo.getReason());
        driverAppointment.setPersonNum(eventInfo.getPersonNum());
        driverAppointment.setPlateNumber(eventInfo.getPlateNumber());
        driverAppointment.setEmiStandard(eventInfo.getEmiStandard());
        driverAppointment.setContactPhone(eventInfo.getPhone());
        driverAppointment.setRemark(eventInfo.getRemark());
        driverAppointment.setDeptId(sysDeptMapper.selectDeptIdByName(eventInfo.getProductionLine()));
        driverAppointment.setUserId(0);
        driverAppointmentMapper.insertDriver(driverAppointment);
        return AjaxResult.success();
    }


}
