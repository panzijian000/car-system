package com.ruoyi.web.controller;

import com.sun.jna.platform.unix.solaris.LibKstat;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

@RestController
public class WeChatController {

    @Value("${wx.appid}")
    private String appid;
    @Value("${wx.secret}")
    private String secret;

    @GetMapping("/getOpenid")
    public String getOpenid(@RequestParam("js_code") String js_code) throws UnsupportedEncodingException {
        String jsCode =js_code;
        String url = "https://api.weixin.qq.com/sns/jscode2session?appid=" +
                URLEncoder.encode(appid, StandardCharsets.UTF_8.toString()) +
                "&secret=" + URLEncoder.encode(secret, StandardCharsets.UTF_8.toString()) +
                "&js_code=" + URLEncoder.encode(jsCode, StandardCharsets.UTF_8.toString()) +
                "&grant_type=authorization_code";
        try {
            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            con.setRequestMethod("GET");

            int responseCode = con.getResponseCode();
            System.out.println("GET Response Code :: " + responseCode);

            if (responseCode == HttpURLConnection.HTTP_OK) { // success
                BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();
                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();

                // 返回解析后的openid，这里返回的是原始JSON字符串
                return response.toString();
            } else {
                return "Failed to get openid: HTTP status code " + responseCode;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return "Error occurred while getting openid: " + e.getMessage();
        }
    }
}