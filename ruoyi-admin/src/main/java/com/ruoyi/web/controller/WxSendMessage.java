package com.ruoyi.web.controller;

import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.ruoyi.common.annotation.Anonymous;
import com.ruoyi.system.domain.Emnu.AppointmentStatus;
import com.ruoyi.system.domain.pojo.DriverAppointment;
import com.ruoyi.system.mapper.DriverAppointmentMapper;
import com.ruoyi.system.mapper.SysDictDataMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@RestController
@Anonymous
public class WxSendMessage {
    @Value("${wx.templateId1}")
    private String templateId1;
    @Value("${wx.templateId2}")
    private String templateId2;
    @Value("${wx.appid}")
    private String appId;
    @Value("${wx.secret}")
    private String secret;

    @Autowired
    DriverAppointmentMapper driverAppointmentMapper;

    @Autowired
    SysDictDataMapper sysDictDataMapper;
    /**
     * 发送消息  调用消息模板方法
     *
     * @return
     */
    @GetMapping("/sendMessages/{id}")
    @Anonymous
    public boolean sendMsg(@PathVariable("id") Integer id) throws UnsupportedEncodingException {
        System.out.println("执行发送消息的id是"+id);
        String token = quertToken();
        DriverAppointment driverAppointment = driverAppointmentMapper.selectDriverById(id);
        if (token != null) {
            if(AppointmentStatus.EXPIRED.getCode()==driverAppointment.getStatus()||AppointmentStatus.REJECTED.getCode()==driverAppointment.getStatus()){
                send2(token,driverAppointmentMapper.selectDriverById(id));
            }
            if (AppointmentStatus.ADMITTED.getCode()==driverAppointment.getStatus()){
                send1(token,driverAppointmentMapper.selectDriverById(id));
            }
        } else {
            System.out.println("获取token失败");
        }
        return true;
    }


    /**
     *     模板消息一   需要 传递openid   以及模板信息(预约成功 消息模板)
     */

    private void send1(String token,DriverAppointment driverAppointment) {
        String msgUrl = "https://api.weixin.qq.com/cgi-bin/message/subscribe/send?access_token=" + token;
        Map<String, Object> paraMap = new HashMap<>();
        paraMap.put("touser",driverAppointment.getOpenid());
        paraMap.put("template_id", templateId1);
//        paraMap.put("page", "pages/index");
        paraMap.put("page", "pages/detail/detail");

        // 假设thing14是模板中的一个数据点
        Map<String, Object> data = new HashMap<>();
        Date contactTime = driverAppointment.getContactTime();
//contactTime加15分钟      变成字符串这种格式  "2019年11月11日 20:00"
// 创建 Calendar 对象并设置为当前时间
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(contactTime);

        // 在 Calendar 对象上添加 15 分钟
        calendar.add(Calendar.MINUTE, sysDictDataMapper.selectOverTimeByDictCode());

        // 创建 SimpleDateFormat 对象，定义日期时间格式
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日 HH:mm");

        // 将更新后的 Calendar 对象转换为字符串
        String formattedDateTime = sdf.format(calendar.getTime());


        data.put("time13", new HashMap<String, Object>() {{
            put("value",formattedDateTime); // 替换为实际的值
        }});
        data.put("car_number3", new HashMap<String, Object>() {{
            put("value", driverAppointment.getPlateNumber()); // 替换为实际的值
        }});
        data.put("phrase1", new HashMap<String, Object>() {{
            put("value", "预约成功"); // 替换为实际的值
        }});
        paraMap.put("data",data);
        String jsonStr = String.valueOf(JSONUtil.parseObj(paraMap));
        String result = HttpUtil.post(msgUrl, jsonStr);
        System.out.println(result);
    }


    /**
     *     模板消息二   需要 传递openid   以及模板信息(预约超时 预约失败)
     */

    private void send2(String token, DriverAppointment driverAppointment) {
        String msgUrl = "https://api.weixin.qq.com/cgi-bin/message/subscribe/send?access_token=" + token;
        Map<String, Object> paraMap = new HashMap<>();
        paraMap.put("touser",driverAppointment.getOpenid());
        paraMap.put("template_id", templateId2);
//        paraMap.put("page", "pages/index");
        paraMap.put("page", "pages/detail/detail");

        // 假设thing14是模板中的一个数据点
        Map<String, Object> data = new HashMap<>();
        data.put("car_number2", new HashMap<String, Object>() {{
            put("value", driverAppointment.getPlateNumber()); // 替换为实际的值
        }});
        String message = "";
        if(AppointmentStatus.REJECTED.getCode()==driverAppointment.getStatus()){
            data.put("thing1", new HashMap<String, Object>() {{

                put("value", "您的预约被拒绝,请联系对接人员"); // 替换为实际的值
            }});
        }

        if(AppointmentStatus.EXPIRED.getCode()==driverAppointment.getStatus()){
            data.put("thing1", new HashMap<String, Object>() {{

                put("value",sysDictDataMapper.selectOverTimeByDictCode()+"分钟内未进入，预约已经作废"); // 替换为实际的值
            }});
        }

        paraMap.put("data",data);
        String jsonStr = String.valueOf(JSONUtil.parseObj(paraMap));
        String result = HttpUtil.post(msgUrl, jsonStr);
        System.out.println(result);
    }

    private String quertToken() throws UnsupportedEncodingException {
        String tokenUrl = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid="
                + URLEncoder.encode(appId, "UTF-8") + "&secret=" + URLEncoder.encode(secret, "UTF-8");
        String result = HttpUtil.get(tokenUrl);
        JSONObject jsonObject = JSONUtil.parseObj(result);
        return jsonObject.getStr("access_token");
    }
}
