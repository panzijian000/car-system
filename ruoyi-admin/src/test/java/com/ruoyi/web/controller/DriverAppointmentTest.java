package com.ruoyi.web.controller;


import com.ruoyi.common.core.domain.entity.SysDept;
import com.ruoyi.system.mapper.SysDeptMapper;
import com.ruoyi.system.mapper.SysDictDataMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import javax.annotation.Resource;
import java.io.UnsupportedEncodingException;
import java.util.List;


@SpringBootTest
class
DriverAppointmentTest {

//     @Value("${wx.openid1}")
//     String openID;

    @Resource
    DriverAppointmentController driverAppointmentController;

    @Autowired
    SysDictDataMapper sysDictDataMapper;

    @Autowired
    SysDeptMapper sysDeptMapper;

    @Autowired
    WxSendMessage wxSendMessage;
    @Autowired
    WeChatController weChatController;

    @Test
    public   void test1()  {

        driverAppointmentController.getDriverListByOpenid("o81d564qXvctzUztHYnhr0HOTk7Y");

    }

    @Test
    public   void test2() throws UnsupportedEncodingException {
        wxSendMessage.sendMsg(33);
    }

    @Test
    public void testname(){
        List<SysDept> sysDepts = sysDeptMapper.selectDeptListNameAndId();
        System.out.println(sysDepts);
    }

    @Test
    public void testidbyname(){

        System.out.println(sysDeptMapper.selectDeptIdByName("市场部门"));
    }

    @Test
    public void testEmiStandardtList(){

        driverAppointmentController.selectEmiStandardtList();
    }

    @Test
    public void  testHuoquOpenid() {
//        System.out.println(weChatController.getOpenid());
//        System.out.println(sysDictDataMapper.selectOverTimeByDictCode());;
        System.out.println(driverAppointmentController.selectReasonList());
    }

}