package com.ruoyi.system.domain.Emnu;

public enum AppointmentStatus {
    // 0 已预约
    SCHEDULED(0, "已预约"),
    // 1 可以进厂
    ADMITTED(3, "可以进厂"),
    // 2 预约不通过
    REJECTED(2, "预约不通过"),
    // 3 超时未进场
    EXPIRED(4, "超时未进场"),
    // 4 进厂
    ENTERED(5, "进厂"),
    // 5 取消预约
    CANCELED(1, "取消预约");

    private final int code;
    private final String description;

    AppointmentStatus(int code, String description) {
        this.code = code;
        this.description = description;
    }

    public int getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }
}