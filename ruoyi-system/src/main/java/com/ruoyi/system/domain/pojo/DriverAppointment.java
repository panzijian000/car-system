package com.ruoyi.system.domain.pojo;

import org.apache.commons.lang3.builder.ToStringBuilder;


import java.util.Date;

public class DriverAppointment {
    private int id;
    private String name;
    private String plateNumber;

    private String company;
    private String contactPhone;

    private int OrderLineTotal;

    private int OrderLine;
    private String targetLine;
    private String reason;
    private int status; // 0: Pending, 1: Confirmed, 2: Cancelled
    private Date createdAt;
    private String openid;

    private int deptId;

    private  int userId;

    private  Date contactTime;

    private String  containerNum;
    private int personNum;


    private  String sealNum;

    private  String remark;

    private String emiStandard;

    public String getEmiStandard() {
        return emiStandard;
    }

    public void setEmiStandard(String emiStandard) {
        this.emiStandard = emiStandard;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", id)
                .append("name", name)
                .append("plateNumber", plateNumber)
                .append("company", company)
                .append("contactPhone", contactPhone)
                .append("OrderLineTotal", OrderLineTotal)
                .append("OrderLine", OrderLine)
                .append("targetLine", targetLine)
                .append("reason", reason)
                .append("status", status)
                .append("createdAt", createdAt)
                .append("openid", openid)
                .append("deptId", deptId)
                .append("userId", userId)
                .append("contactTime", contactTime)
                .append("containerNum", containerNum)
                .append("personNum", personNum)
                .append("sealNum", sealNum)
                .append("remark", remark)
                .toString();
    }

    public int getPersonNum() {
        return personNum;
    }

    public void setPersonNum(int personNum) {
        this.personNum = personNum;
    }

    public String getSealNum() {
        return sealNum;
    }

    public void setSealNum(String sealNum) {
        this.sealNum = sealNum;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getContainerNum() {
        return containerNum;
    }

    public void setContainerNum(String containerNum) {
        this.containerNum = containerNum;
    }

    public Date getContactTime() {
        return contactTime;
    }

    public void setContactTime(Date contactTime) {
        this.contactTime = contactTime;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getDeptId() {
        return deptId;
    }

    public void setDeptId(int deptId) {
        this.deptId = deptId;
    }

    // Constructors
    public DriverAppointment() {
    }

    public DriverAppointment(String name, String plateNumber, String contactPhone, String targetLine, String reason, int status) {
        this.name = name;
        this.plateNumber = plateNumber;
        this.contactPhone = contactPhone;
        this.targetLine = targetLine;
        this.reason = reason;
        this.status = status;
        this.createdAt = new Date(); // Automatically set the creation time
    }

    public int getOrderLineTotal() {
        return OrderLineTotal;
    }

    public void setOrderLineTotal(int orderLineTotal) {
        OrderLineTotal = orderLineTotal;
    }

    public int getOrderLine() {
        return OrderLine;
    }

    public void setOrderLine(int orderLine) {
        OrderLine = orderLine;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid;
    }

    // Getters and Setters
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPlateNumber() {
        return plateNumber;
    }

    public void setPlateNumber(String plateNumber) {
        this.plateNumber = plateNumber;
    }

    public String getContactPhone() {
        return contactPhone;
    }

    public void setContactPhone(String contactPhone) {
        this.contactPhone = contactPhone;
    }

    public String getTargetLine() {
        return targetLine;
    }

    public void setTargetLine(String targetLine) {
        this.targetLine = targetLine;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }


}