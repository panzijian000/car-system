package com.ruoyi.system.domain.pojo;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class EventInfo {
    private  int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    private String company;
    private String openid;
    private String phone;
    private String position;
    private String productionLine;

    private int dept_id;

    private int personNum;

    private String  containerNum;

    private String plateNumber;

    private  String remark;

    private  String sealNum;

    private String emiStandard;

    public String getEmiStandard() {
        return emiStandard;
    }

    public void setEmiStandard(String emiStandard) {
        this.emiStandard = emiStandard;
    }

    public String getContainerNum() {
        return containerNum;
    }

    public void setContainerNum(String containerNum) {
        this.containerNum = containerNum;
    }

    public String getPlateNumber() {
        return plateNumber;
    }

    public void setPlateNumber(String plateNumber) {
        this.plateNumber = plateNumber;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getSealNum() {
        return sealNum;
    }

    public void setSealNum(String sealNum) {
        this.sealNum = sealNum;
    }


    public int getPersonNum() {
        return personNum;
    }

    public void setPersonNum(int personNum) {
        this.personNum = personNum;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", id)
                .append("company", company)
                .append("openid", openid)
                .append("phone", phone)
                .append("position", position)
                .append("productionLine", productionLine)
                .append("dept_id", dept_id)
                .append("personNum", personNum)
                .append("containerNum", containerNum)
                .append("plateNumber", plateNumber)
                .append("remark", remark)
                .append("sealNum", sealNum)
                .append("reason", reason)
                .append("name", name)
                .toString();
    }

    public int getDept_id() {
        return dept_id;
    }

    public void setDept_id(int dept_id) {
        this.dept_id = dept_id;
    }

    private String reason;
    private String name;

    // 构造函数
    public EventInfo() {
    }

    // Getter和Setter方法
    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getProductionLine() {
        return productionLine;
    }

    public void setProductionLine(String productionLine) {
        this.productionLine = productionLine;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getUsername() {
        return name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setUsername(String name) {
        this.name = name;
    }

}