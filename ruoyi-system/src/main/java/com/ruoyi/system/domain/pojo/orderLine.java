package com.ruoyi.system.domain.pojo;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class orderLine {
    private int orderLineTotal;

    private int orderLine;

    private String targetLine;
    private String reason;

    public int getOrderLineTotal() {
        return orderLineTotal;
    }

    public void setOrderLineTotal(int orderLineTotal) {
        this.orderLineTotal = orderLineTotal;
    }

    public int getOrderLine() {
        return orderLine;
    }

    public void setOrderLine(int orderLine) {
        this.orderLine = orderLine;
    }

    public String getTargetLine() {
        return targetLine;
    }

    public void setTargetLine(String targetLine) {
        this.targetLine = targetLine;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("orderLineTotal", orderLineTotal)
                .append("orderLine", orderLine)
                .append("targetLine", targetLine)
                .append("reason", reason)
                .toString();
    }
}
