package com.ruoyi.system.mapper;


import com.ruoyi.system.domain.Emnu.AppointmentStatus;
import com.ruoyi.system.domain.pojo.DriverAppointment;
import com.ruoyi.system.domain.pojo.EventInfo;
import com.ruoyi.system.domain.pojo.orderLine;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;


public interface   DriverAppointmentMapper {

//    获取所有预约集合
    List<DriverAppointment> DriverList();

//    新增预约记录
    int insertDriver(DriverAppointment driverAppointment);

//    根据openId 获取用户集合
    List<DriverAppointment> DriverListByOpenId(@Param("openid") String openid);

//    根据id集合 批量修改状态
    void batchUpdateStatus(@Param("expireOrderIds") List<Integer> expiredOrderIds,@Param("status") int overtime);

//    根据id 改变状态
   int updateStatusById(@Param("id") int id,@Param("status") int status);

//   根据id查询
    DriverAppointment selectDriverById(@Param("id") int id);

    int updateById(@Param("info") EventInfo eventInfo);

//    返回总数据
    List<orderLine> totalCountByCondition();


    orderLine countByCondtion(@Param("time") Date createdAt, @Param("targetLine") String targetLine, @Param("reason") String reason);
}
